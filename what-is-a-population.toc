\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}%
\contentsline {section}{\numberline {2}Populations and population definition}{3}{section.2}%
\contentsline {section}{\numberline {3}Census and span}{3}{section.3}%
\contentsline {section}{\numberline {4}Individuals and events}{4}{section.4}%
\contentsline {section}{\numberline {5}Subpopulations}{5}{section.5}%
\contentsline {section}{\numberline {6}Cohorts}{6}{section.6}%
\contentsline {section}{\numberline {7}Population equations}{6}{section.7}%
\contentsline {section}{\numberline {8}Production and Reproduction}{8}{section.8}%
\contentsline {section}{\numberline {9}Conclusion}{9}{section.9}%
\contentsline {section}{\numberline {10}Acknowledgements}{9}{section.10}%
\contentsline {section}{\hbox to\@tempdima {\hfil }References}{10}{section.10}%
